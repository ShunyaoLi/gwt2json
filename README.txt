1. 生成json时用到了gson包。lib文件夹里有gson-2.7.jar。
2. 本程序的功能：
  1）将txt格式的GWT文档转化为json文档
  2）识别GWT文档中GWT关键词的拼写错误并报错，报错信息如下：
    line:15  error:关键词I want to拼写错误
  3）当GWT文档中存在关键词缺失时，json文档中相应位置也表现为缺失。具体缺失格式如下：
    ① 除example外，其余关键词均为Pair类型，其缺失表现为
	"scenario_text": {
            "lineNumber": 0,
            "content": ""
        }
	"then": {
            "lineNumber": 0,
            "content": ""
        }
    ② example为ArrayList类型，其缺失表现为
	"example": []
3. 可能需要重写的接口
  1）19行GWT文件路径
  2）47-54行打开GWT文件
  3）77,95，...，235等行的报错信息
  4）330行输出json
