package gwt2json;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GWT2JSON {
	// 记录前一行的文本内容属于哪个关键词
	enum prevLineTest {  
		  Feature, As_a, I_want_to, In_order_to, Scenario, Given, When, Then, Example, None
	}  
	public static void main(String[] args) {
		// txt格式GWT文件的路径
		String pathname = "C:\\Users\\dana\\Desktop\\u.txt";
		// GWT模糊关键词正则表达式，用于检查GWT关键词拼写错误
		Pattern fuzzyFeature = Pattern.compile("f\\w{0,4}[tril1|][uae][rtil1|]e?\\||f\\w{0,4}[tril1|][uae][rtil1|]e?\\w*", Pattern.CASE_INSENSITIVE);
		Pattern fuzzyAs_a = Pattern.compile("as\\s*a?", Pattern.CASE_INSENSITIVE);
		Pattern fuzzyI_want_to = Pattern.compile("[i1l|]?\\s*want\\s*\\w{0,4}", Pattern.CASE_INSENSITIVE);
		Pattern fuzzyIn_order_to = Pattern.compile("[i1l|]n\\s*o[ri1l|]d\\w{0,3}\\s*\\w{0,3}", Pattern.CASE_INSENSITIVE);
		Pattern fuzzyScenario = Pattern.compile("s\\w{1,6}[aeu][ril1|][i1l|][o0e]\\w*\\s?outline\\s?\\w*|s\\w{1,6}[aeu][ril1|][i1l|][o0e]\\||s\\w{1,6}[aeu][ril1|][i1l|][o0e]\\w*", Pattern.CASE_INSENSITIVE);
		Pattern fuzzyGiven = Pattern.compile("g\\s?[i1l|]\\s?v\\s?e\\s?n?", Pattern.CASE_INSENSITIVE);
		Pattern fuzzyWhen = Pattern.compile("w[htril1|{}()\\[\\]]\\w?[euao0]\\w{0,3}", Pattern.CASE_INSENSITIVE);
		Pattern fuzzyThen = Pattern.compile("t[htril1|{}()\\[\\]]\\w?[euao0]\\w{0,3}", Pattern.CASE_INSENSITIVE);
		Pattern fuzzyExample = Pattern.compile("ex[aeu]m[po]les?", Pattern.CASE_INSENSITIVE);
		// GWT正确关键词正则表达式
		Pattern correctFeature = Pattern.compile("[Ff]eature\\d*");
		Pattern correctAs_a = Pattern.compile("[Aa]s\\sa");
		Pattern correctI_want_to = Pattern.compile("[Ii]\\swant\\sto");
		Pattern correctIn_order_to = Pattern.compile("[Ii]n\\sorder\\sto");
		Pattern correctScenario = Pattern.compile("[Ss]cenario\\d*");
		Pattern correctGiven = Pattern.compile("[Gg]iven");
		Pattern correctWhen = Pattern.compile("[Ww]hen");
		Pattern correctThen = Pattern.compile("[Tt]hen");
		Pattern correctExample = Pattern.compile("[Ee]xample");
		int lineNumber = 0; // 初始化行号
		GWT gwt = new GWT();
		Feature tmpFeature = new Feature();
		Event tmpEvent = new Event();
		Scenario tmpScenario = new Scenario();
		prevLineTest prevLine = prevLineTest.None;
		// 打开txt格式GWT文件
		FileReader fr = null;
		try {
			fr = new FileReader(pathname);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(fr);
		String line;
		try {
			// 按行读取文件
			while ((line = br.readLine()) != null) {
				lineNumber++;
				line = line.trim(); // 去除当前行字符串左侧和右侧的空格
				//System.out.println(line); // 打印当前行
				if (line == null || line.length() <= 0) continue; // 若当前行为空，继续读下一行
				// 在当前行中匹配模糊关键词
				Matcher matcherFeature = fuzzyFeature.matcher(line);
				Matcher matcherAs_a = fuzzyAs_a.matcher(line);
				Matcher matcherI_want_to = fuzzyI_want_to.matcher(line);
				Matcher matcherIn_order_to = fuzzyIn_order_to.matcher(line);
				Matcher matcherScenario = fuzzyScenario.matcher(line);
				Matcher matcherGiven = fuzzyGiven.matcher(line);
				Matcher matcherWhen = fuzzyWhen.matcher(line);
				Matcher matcherThen = fuzzyThen.matcher(line);
				Matcher matcherExample = fuzzyExample.matcher(line);
				// 若匹配到模糊关键词Feature
				if (matcherFeature.find()) {
					// 若匹配部分不是正确关键词
					if (!correctFeature.matcher(matcherFeature.group()).matches()) {
						System.out.println("line:" + lineNumber + "  error:关键词Feature拼写错误"); // 报错，输出行号及拼写错误信息
						line = line.replace(line.substring(matcherFeature.start(), matcherFeature.end()), "Feature"); // 将拼写错误关键词改正
					}
					prevLine = prevLineTest.Feature; // 设置当前行文本属于关键词Feature
					gwt.feature.add(tmpFeature); // 在feature数组中新建一项
					tmpFeature = new Feature();
					Feature feature = gwt.feature.get(gwt.feature.size() - 1); // 取出feature数组中最后一项
					feature.feature_text.setFirst(lineNumber); // 设置feature_text的行号
					int index = 7;
					if (line.length() <= index + 1) continue; // 若当前行仅由关键词组成，直接读下一行
					// 忽略关键词后的标点符号及数字
					while (line.charAt(index) == ':' || line.charAt(index) == '：' || line.charAt(index) == ' ' || line.charAt(index) == 'l' || (line.charAt(index) >= 48 && line.charAt(index) <= 57))
						index++;
					feature.feature_text.setSecond(line.substring(index)); // 设置feature_text的内容
				}
				// 若匹配到模糊关键词As a
				else if (matcherAs_a.find()) {
					if (!correctAs_a.matcher(matcherAs_a.group()).matches()) {
						System.out.println("line:" + lineNumber + "  error:关键词As a拼写错误");
						line = line.replace(line.substring(matcherAs_a.start(), matcherAs_a.end()), "As a");
					}
					prevLine = prevLineTest.As_a;
					// 若feature数组为空或者feature数组中最后一项内容（除了feature_text）不为空，表明之前发生了关键词Feature缺失的情况
					if(gwt.feature.size() == 0 || gwt.feature.get(gwt.feature.size() - 1).isEmptyAs_a() == false){
						gwt.feature.add(tmpFeature); // 在feature数组中新建一项
						tmpFeature = new Feature();
					}
					Feature feature = gwt.feature.get(gwt.feature.size() - 1);
					feature.as_a.setFirst(lineNumber);
					int index = 4;
					if (line.length() <= index + 1) continue;
					while (line.charAt(index) == ':' || line.charAt(index) == '：' || line.charAt(index) == ' ' || line.charAt(index) == 'l' || (line.charAt(index) >= 48 && line.charAt(index) <= 57))
						index++;
					feature.as_a.setSecond(line.substring(index));
				}
				// 若匹配到模糊关键词I want to
				else if (matcherI_want_to.find()) {
					if (!correctI_want_to.matcher(matcherI_want_to.group()).matches()) {
						System.out.println("line:" + lineNumber + "  error:关键词I want to拼写错误");
						line = line.replace(line.substring(matcherI_want_to.start(), matcherI_want_to.end()), "I want to");
					}
					prevLine = prevLineTest.I_want_to;
					// 之前发生了关键词Feature缺失的情况
					if(gwt.feature.size() == 0 || gwt.feature.get(gwt.feature.size() - 1).isEmptyI_want_to() == false){
						gwt.feature.add(tmpFeature); // 在feature数组中新建一项
						tmpFeature = new Feature();
					}
					Feature feature = gwt.feature.get(gwt.feature.size() - 1);
					feature.i_want_to.setFirst(lineNumber);
					int index = 9;
					if (line.length() <= index + 1) continue;
					while (line.charAt(index) == ':' || line.charAt(index) == '：' || line.charAt(index) == ' ' || line.charAt(index) == 'l' || (line.charAt(index) >= 48 && line.charAt(index) <= 57))
						index++;
					feature.i_want_to.setSecond(line.substring(index));
				}
				// 若匹配到模糊关键词In order to
				else if (matcherIn_order_to.find()) {
					if (!correctIn_order_to.matcher(matcherIn_order_to.group()).matches()) {
						System.out.println("line:" + lineNumber + "  error:关键词In order to拼写错误");
						line = line.replace(line.substring(matcherIn_order_to.start(), matcherIn_order_to.end()), "In order to");
					}
					prevLine = prevLineTest.In_order_to;
					// 之前发生了关键词Feature缺失的情况
					if(gwt.feature.size() == 0 || gwt.feature.get(gwt.feature.size() - 1).isEmptyIn_order_to() == false){
						gwt.feature.add(tmpFeature); // 在feature数组中新建一项
						tmpFeature = new Feature();
					}
					Feature feature = gwt.feature.get(gwt.feature.size() - 1);
					feature.in_order_to.setFirst(lineNumber);
					int index = 11;
					if (line.length() <= index + 1) continue;
					while (line.charAt(index) == ':' || line.charAt(index) == '：' || line.charAt(index) == ' ' || line.charAt(index) == 'l' || (line.charAt(index) >= 48 && line.charAt(index) <= 57))
						index++;
					feature.in_order_to.setSecond(line.substring(index));
				}
				// 若匹配到模糊关键词Scenario
				else if (matcherScenario.find()) {
					if (!correctScenario.matcher(matcherScenario.group()).matches()) {
						System.out.println("line:" + lineNumber + "  error:关键词Scenario拼写错误");
						line = line.replace(line.substring(matcherScenario.start(), matcherScenario.end()), "Scenario");
					}
					prevLine = prevLineTest.Scenario;
					Feature feature = gwt.feature.get(gwt.feature.size() - 1);
					feature.scenario.add(tmpScenario); // 在scenario数组中新建一项
					tmpScenario = new Scenario();
					Scenario scenario = feature.scenario.get(feature.scenario.size() - 1);
					scenario.scenario_text.setFirst(lineNumber);
					int index = 8;
					if (line.length() <= index + 1) continue;
					while (line.charAt(index) == ':' || line.charAt(index) == '：' || line.charAt(index) == ' ' || line.charAt(index) == 'l' || (line.charAt(index) >= 48 && line.charAt(index) <= 57))
						index++;
					scenario.scenario_text.setSecond(line.substring(index));
				}
				// 若匹配到模糊关键词Given
				else if (matcherGiven.find()) {	
					if (!correctGiven.matcher(matcherGiven.group()).matches()) {
						System.out.println("line:" + lineNumber + "  error:关键词Given拼写错误");
						line = line.replace(line.substring(matcherGiven.start(), matcherGiven.end()), "Given");
					}
					prevLine = prevLineTest.Given;
					Feature feature = gwt.feature.get(gwt.feature.size() - 1);
					// 若scenario数组为空或者scenario数组中最后一项内容（除了scenario_text）不为空，表明之前发生了关键词Scenario缺失的情况
					if(feature.scenario.size() == 0 || feature.scenario.get(feature.scenario.size() - 1).isEmptyGiven() == false){
						feature.scenario.add(tmpScenario); // 在scenario数组中新建一项
						tmpScenario = new Scenario();
					}
					Scenario scenario = feature.scenario.get(feature.scenario.size() - 1);
					scenario.given.setFirst(lineNumber);
					int index = 5;
					if (line.length() <= index + 1) continue;
					while (line.charAt(index) == ':' || line.charAt(index) == '：' || line.charAt(index) == ' ' || line.charAt(index) == 'l' || (line.charAt(index) >= 48 && line.charAt(index) <= 57))
						index++;
					scenario.given.setSecond(line.substring(index));
				}
				// 若匹配到模糊关键词When
				else if (matcherWhen.find()) {
					if (!correctWhen.matcher(matcherWhen.group()).matches()) {
						System.out.println("line:" + lineNumber + "  error:关键词When拼写错误");
						line = line.replace(line.substring(matcherWhen.start(), matcherWhen.end()), "When");
					}
					prevLine = prevLineTest.When;
					Feature feature = gwt.feature.get(gwt.feature.size() - 1);
					Scenario scenario = feature.scenario.get(feature.scenario.size() - 1);
					scenario.event.add(tmpEvent); // 在event数组中新建一项
					tmpEvent = new Event();
					Event event = scenario.event.get(scenario.event.size() - 1);
					event.when.setFirst(lineNumber);
					int index = 4;
					if (line.length() <= index + 1) continue;
					while (line.charAt(index) == ':' || line.charAt(index) == '：' || line.charAt(index) == ' ' || line.charAt(index) == 'l' || (line.charAt(index) >= 48 && line.charAt(index) <= 57))
						index++;
					event.when.setSecond(line.substring(index));
				}
				// 若匹配到模糊关键词Then
				else if (matcherThen.find()) {
					if (!correctThen.matcher(matcherThen.group()).matches()) {
						System.out.println("line:" + lineNumber + "  error:关键词Then拼写错误");
						line = line.replace(line.substring(matcherThen.start(), matcherThen.end()), "Then");
					}
					prevLine = prevLineTest.Then;
					int index = 4;
					Feature feature = gwt.feature.get(gwt.feature.size() - 1);
					Scenario scenario = feature.scenario.get(feature.scenario.size() - 1);
					// 若event数组为空或者event数组中最后一项的then不为空，表明之前发生了关键词when缺失的情况
					if(scenario.event.size() == 0 || scenario.event.get(scenario.event.size() - 1).isEmptyThen() == false){
						scenario.event.add(tmpEvent); // 在event数组中新建一项
						tmpEvent = new Event();
					}
					Event event = scenario.event.get(scenario.event.size() - 1);
					event.then.setFirst(lineNumber);
					if (line.length() <= index + 1) continue;
					while (line.charAt(index) == ':' || line.charAt(index) == '：' || line.charAt(index) == ' ' || line.charAt(index) == 'l' || (line.charAt(index) >= 48 && line.charAt(index) <= 57))
						index++;
					event.then.setSecond(line.substring(index));
				}
				// 若匹配到模糊关键词Example
				else if (matcherExample.find()) {
					if (!correctExample.matcher(matcherExample.group()).matches()) {
						System.out.println("line:" + lineNumber + "  error:关键词Example拼写错误");
						line = line.replace(line.substring(matcherExample.start(), matcherExample.end()), "Example");
					}
					prevLine = prevLineTest.Example;
					int index = 7;
					if (line.length() <= index + 1) continue;
					while (line.charAt(index) == ':' || line.charAt(index) == '：' || line.charAt(index) == ' ' || line.charAt(index) == 'l' || (line.charAt(index) >= 48 && line.charAt(index) <= 57)) 
						index++;
					Pair tmpPair = new Pair();
					tmpPair.setFirst(lineNumber);
					tmpPair.setSecond(line.substring(index));
					Feature feature = gwt.feature.get(gwt.feature.size() - 1);
					Scenario scenario = feature.scenario.get(feature.scenario.size() - 1);
					scenario.example.add(tmpPair);
				}
				// 当前行没有关键词，查看上一行的关键词
				else{ 
					 // 若上一行无关键词，则抛弃当前行
					if(prevLine == prevLineTest.None){
						continue;
					}
					 // 若上一行关键词为Feature，将当前行内容添加到feature_text末尾
					else if(prevLine == prevLineTest.Feature){
						Feature feature = gwt.feature.get(gwt.feature.size() - 1);
						String tmpPrev = feature.feature_text.getSecond();
						tmpPrev += line;
						feature.feature_text.setSecond(tmpPrev);
					}
					else if(prevLine == prevLineTest.As_a){
						Feature feature = gwt.feature.get(gwt.feature.size() - 1);
						String tmpPrev = feature.as_a.getSecond();
						tmpPrev += line;
						feature.as_a.setSecond(tmpPrev);
					}
					else if(prevLine == prevLineTest.I_want_to){
						Feature feature = gwt.feature.get(gwt.feature.size() - 1);
						String tmpPrev = feature.i_want_to.getSecond();
						tmpPrev += line;
						feature.i_want_to.setSecond(tmpPrev);
					}
					else if(prevLine == prevLineTest.In_order_to){
						Feature feature = gwt.feature.get(gwt.feature.size() - 1);
						String tmpPrev = feature.in_order_to.getSecond();
						tmpPrev += line;
						feature.in_order_to.setSecond(tmpPrev);
					}
					else if(prevLine == prevLineTest.Scenario){
						Feature feature = gwt.feature.get(gwt.feature.size() - 1);
						Scenario scenario = feature.scenario.get(feature.scenario.size() - 1);
						String tmpPrev = scenario.scenario_text.getSecond();
						tmpPrev += line;
						scenario.scenario_text.setSecond(tmpPrev);
					}
					else if(prevLine == prevLineTest.Given){
						Feature feature = gwt.feature.get(gwt.feature.size() - 1);
						Scenario scenario = feature.scenario.get(feature.scenario.size() - 1);
						String tmpPrev = scenario.given.getSecond();
						tmpPrev += line;
						scenario.given.setSecond(tmpPrev);
					}
					else if(prevLine == prevLineTest.When){
						Feature feature = gwt.feature.get(gwt.feature.size() - 1);
						Scenario scenario = feature.scenario.get(feature.scenario.size() - 1);
						Event event = scenario.event.get(scenario.event.size() - 1);
						String tmpPrev = event.when.getSecond();
						tmpPrev += line;
						event.when.setSecond(tmpPrev);
					}
					else if(prevLine == prevLineTest.Then){
						Feature feature = gwt.feature.get(gwt.feature.size() - 1);
						Scenario scenario = feature.scenario.get(feature.scenario.size() - 1);
						Event event = scenario.event.get(scenario.event.size() - 1);
						String tmpPrev = event.then.getSecond();
						tmpPrev += line;
						event.then.setSecond(tmpPrev);
					}
					else if(prevLine == prevLineTest.Example){
						Pair tmpPair = new Pair();
						tmpPair.setFirst(lineNumber);
						tmpPair.setSecond(line);
						Feature feature = gwt.feature.get(gwt.feature.size() - 1);
						Scenario scenario = feature.scenario.get(feature.scenario.size() - 1);
						scenario.example.add(tmpPair);
					}
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		// 新建Gson对象，其中disableHtmlEscaping()作用是正常显示'<','>'等符号，setPrettyPrinting()作用是将json排好格式
		Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
		// 将GWT对象转化为json
		String jsonStr = gson.toJson(gwt);
		// 输出json
		System.out.println(jsonStr);
	}
}  