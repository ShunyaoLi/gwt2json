package gwt2json;

public class Pair {
	private Integer lineNumber;
	private String content;
	public Pair() {
		lineNumber = 0;
		content = "";
	}
	public Integer getFirst() {
		return lineNumber;
	}
	public void setFirst(Integer first) {
		this.lineNumber = first;
	}
	public String getSecond() {
		return content;
	}
	public void setSecond(String second) {
		this.content = second;
	}
	public boolean isEmpty(){
		if(lineNumber == 0 && content == "") return true;
		else return false;
	}
}
