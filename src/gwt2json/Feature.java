package gwt2json;

import java.util.ArrayList;

public class Feature {
	Pair feature_text = new Pair();
	Pair as_a = new Pair();
	Pair i_want_to = new Pair();
	Pair in_order_to = new Pair();
	ArrayList<Scenario> scenario = new ArrayList<Scenario>();
	public boolean isEmptyAs_a(){
		if(as_a.isEmpty() == true && i_want_to.isEmpty() == true && in_order_to.isEmpty() == true && scenario.size() == 0) return true;
		else return false;
	}
	public boolean isEmptyI_want_to(){
		if(i_want_to.isEmpty() == true && in_order_to.isEmpty() == true && scenario.size() == 0) return true;
		else return false;
	}
	public boolean isEmptyIn_order_to(){
		if(in_order_to.isEmpty() == true && scenario.size() == 0) return true;
		else return false;
	}
	public boolean isEmptyScenario(){
		if(scenario.size() == 0) return true;
		else return false;
	}
}
